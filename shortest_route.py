import itertools

filename = 'input.txt'
routes_with_dist = {}
cities = []
shortest_dist = 9000000
actual_path = set()
shortest_path = ''

# Extracts the contents of the file distances.txt & stores them in routes_with_dictionary.
# Extracts all the location & stores them in cities set.
with open(filename) as dist_file:
    for line in dist_file:
        (key, value) = line.split("=")
        city1, city2 = key.split(" to ")[0].strip(), key.split(" to ")[1].strip()
        routes_with_dist[city1 + city2] = int(value.strip())
        routes_with_dist[city2 + city1] = int(value.strip())
        cities.append(city1)
        cities.append(city2)
cities = set(cities)

# iterates through the permuated tuple of cities.
for route in itertools.permutations(cities):
    actual_dist = 0

    # iterates through all the navigable cities (to-and-fro)
    # and adds the distances between all the cities
    for city1, city2 in zip(route[:-1], route[1:]):
        actual_dist += routes_with_dist[city1 + city2]

    # checks for the shortest distance among all the distances &
    # assigns it to shortest_dist with the path in actual_path
    if actual_dist < shortest_dist:
        shortest_dist = actual_dist
        actual_path = route

# formats the actual_path to be displayed & assigns to shortest_path
for path in actual_path:
    shortest_path += str(path)
    if path != actual_path[-1]:
        shortest_path += ' -> '

print("Shortest Path:\n\t" + shortest_path)
print("Shortest Distance: " + str(shortest_dist) + "kms")

